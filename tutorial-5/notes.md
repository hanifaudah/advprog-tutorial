# Teaching Assistant Information System Requirements

### Mata Kuliah Requirements
Attributes:
```
MataKuliah

KodeMatkul: String (Primary Key)
namaMatkul: String
prodi: String
```
1. Dapat menyimpan banyak mata kuliah
2. Satu mata kuliah dapat menerima banyak mahasiswa
3. Mata kuliah yang sudah ada asisten tidak dapat dihapus

**Endpoints**
```
(POST) /mata-kuliah/
(GET/PATCH/DELETE) /mata-kuliah/{kodeMatkul}/
Payload (GET/POST/PATCH):
{
    kodeMatkul: String,
    nama: String,
    prodi: String,
}
(POST) /mata-kuliah/daftar-asisten/{npm}/{kodeMatkul}/
```
### Mahasiswa Requirements
Attributes:
```
Mahasiswa

NPM: String (Primary Key)
Nama : String
Email: String
ipk: String
noTelp: String
```
1. Mahasiswa yang sudah menjadi asisten tidak dapat dihapus
2. Mahasiswa dapat CREATE, UPDATE, dan DELETE suatu log
3. Satu mahasiswa hanya dapat menjadi asisten di satu mata kuliah
4. Setiap mahasiswa yang mendaftar langsung diterima

**Endpoints**
```
(POST) /mahasiswa/
(GET/PATCH/DELETE) /mahasiswa/{npm}/
Payload (GET/POST/PATCH):
{
    npm: String,
    nama : String,
    email: String,
    ipk: String,
    noTelp: String,
}
```

### Log Requirements
Attributes:
```
Log

idLog: integer(Primary Key, auto increment)
start: datetime
end: datetime
Deskripsi: Text
```
1. Endpoint untuk melihat log pada bulan tertentu
2. Gaji mahasiswa adalah 350 Greil/jam
3. Satu kali log mungkin tidak sampai 1 jam

**Endpoints**
```
(POST) /log/mahasiswa/{npm}/
(GET/PATCH/DELETE) /log/{idLog}/
Payload (POST/PATCH):
{
    idLog: integer,
    start: datetime,
    endTime: datetime,
    Deskripsi: Text,
}
(GET) /log/mahasiswa/{npm}/
Payload (GET):
[
   {
      "month":"January",
      "jamKerja":10,
      "pembayaran":3500
   },
   {
      "month":"February",
      "jamKerja":40,
      "pembayaran":14000
   }
]
```
