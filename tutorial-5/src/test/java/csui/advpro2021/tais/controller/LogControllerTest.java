package csui.advpro2021.tais.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import csui.advpro2021.tais.service.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(controllers = LogController.class)
class LogControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogRepository logRepository;

    @MockBean
    private LogService logService;

    @MockBean
    private MataKuliahRepository mataKuliahRepository;

    @MockBean
    private MataKuliahService mataKuliahService;

    @MockBean
    private MahasiswaRepository mahasiswaRepository;

    @MockBean
    private MahasiswaService mahasiswaService;

    private MataKuliah matkul;
    private Mahasiswa mahasiswa;
    private Log log;

    @BeforeEach
    public void setUp() throws ParseException {
        mahasiswa = new Mahasiswa("1906293070", "Orang", "orang.ui.ac.id", "4", "087777777777");
        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
        DateFormat format = new SimpleDateFormat("d-M-yyyy", Locale.ENGLISH);
        Date start = format.parse("03-03-2021");
        Date endTime = format.parse("07-03-2021");
        log = new Log(start, endTime, "deskripsi");
    }

    private String mapToJson(Object obj) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(obj);
    }

    @Test
    void testControllerGetValidLog() throws Exception {
        logService.createLog(mahasiswa, log);
        when(logService.getLogById(log.getIdLog())).thenReturn(log);
        mvc.perform(get("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idLog").value(log.getIdLog()));
    }

    @Test
    void testControllerGetInValidLog() throws Exception {
        mvc.perform(get("/log/-1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerGetMahasiswaLogs() throws Exception {
        mahasiswaService.createMahasiswa(mahasiswa);
        mataKuliahService.createMataKuliah(matkul);

        mahasiswa.setMataKuliah(matkul);
        logService.createLog(mahasiswa, log);

        // Expected response
        List<HashMap> responseList = new ArrayList<>();
        HashMap<String, Object> responseItem = new HashMap<>();
        double jamKerja = ((log.getEndTime().getTime() - log.getStart().getTime())/(60 * 60 * 1000.0));
        DateFormat format = new SimpleDateFormat("MMMMM", Locale.ENGLISH);
        responseItem.put("month", format.format(log.getStart()));
        responseItem.put("jamKerja", jamKerja);
        responseItem.put("pembayaran", jamKerja * 350);
        responseList.add(responseItem);

        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        when(logService.getMahasiswaLogs(mahasiswa)).thenReturn(responseList);
        mvc.perform(get("/log/mahasiswa/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].month").value("March"))
                .andExpect(jsonPath("$[0].jamKerja").value(jamKerja))
                .andExpect(jsonPath("$[0].pembayaran").value(jamKerja * 350));
    }

    @Test
    void testControllerGetInValidMahasiswaLogs() throws Exception {
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(null);
        mvc.perform(get("/log/mahasiswa/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerPostLogValidMahasiswa() throws Exception {
        mahasiswaService.createMahasiswa(mahasiswa);
        when(mahasiswaService.getMahasiswaByNPM(mahasiswa.getNpm())).thenReturn(mahasiswa);
        mvc.perform(post("/log/mahasiswa/" + mahasiswa.getNpm()).contentType(MediaType.APPLICATION_JSON).content(mapToJson(log)))
                .andExpect(status().isOk());
    }

    @Test
    void testControllerPostLogInvalidMahasiswa() throws Exception {
        mvc.perform(post("/log/mahasiswa/0000").contentType(MediaType.APPLICATION_JSON).content(mapToJson(log)))
                .andExpect(status().isNotFound());
    }

    @Test
    void testControllerUpdateLog() throws Exception {
        logService.createLog(mahasiswa, log);

        String deskripsiBaru = "deskripsiBaru";
        log.setDeskripsi(deskripsiBaru);

        when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);

        mvc.perform(put("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON)
                .content(mapToJson(log)))
                .andExpect(status().isOk()).andExpect(content()
                .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.deskripsi").value(deskripsiBaru));
    }

    @Test
    void testControllerDeleteLog() throws Exception {
        logService.createLog(mahasiswa, log);
        mvc.perform(delete("/log/" + log.getIdLog()).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

}