package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class MataKuliahServiceImplTest {
    @Mock
    private MataKuliahRepository mataKuliahRepository;

    @InjectMocks
    private MataKuliahServiceImpl mataKuliahService;

    @Mock
    private MahasiswaServiceImpl mahasiswaService;

    private MataKuliah matkul;
    private Mahasiswa mahasiswa;

    @BeforeEach
    public void setUp() {
        matkul = new MataKuliah();
        matkul.setKodeMatkul("ADVPROG");
        matkul.setNama("Advanced Programming");
        matkul.setProdi("Ilmu Komputer");

        mahasiswa = new Mahasiswa("1906293070", "Orang", "orang.ui.ac.id", "4", "087777777777");
    }

    @Test
    void testServiceGetListMataKuliah() {
        Iterable<MataKuliah> listMataKuliah = mataKuliahRepository.findAll();
        lenient().when(mataKuliahService.getListMataKuliah()).thenReturn(listMataKuliah);
        Iterable<MataKuliah> listMataKuliahResult = mataKuliahService.getListMataKuliah();
        Assertions.assertIterableEquals(listMataKuliah, listMataKuliahResult);
    }

    @Test
    void testServiceGetMataKuliah() {
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        MataKuliah resultMatkul = mataKuliahService.getMataKuliah(matkul.getKodeMatkul());
        Assertions.assertEquals(matkul.getKodeMatkul(), resultMatkul.getKodeMatkul());
    }

    @Test
    void testServiceCreateMataKuliah() {
        lenient().when(mataKuliahService.createMataKuliah(matkul)).thenReturn(matkul);
        MataKuliah resultMatkul = mataKuliahService.createMataKuliah(matkul);
        Assertions.assertEquals(matkul.getKodeMatkul(), resultMatkul.getKodeMatkul());
    }

    @Test
    void testServiceUpdateMataKuliah() {
        mataKuliahService.createMataKuliah(matkul);
        String namaMatkul = "ADV125YIHA";
        matkul.setNama(namaMatkul);
        MataKuliah expectedMatkul = matkul;
        expectedMatkul.setNama(namaMatkul);
        MataKuliah resultMatkul = mataKuliahService.updateMataKuliah(matkul.getKodeMatkul(), matkul);
        Assertions.assertEquals(expectedMatkul.getNama(), resultMatkul.getNama());
    }

    @Test
    void testServiceDeleteMataKuliah() {
        mataKuliahService.createMataKuliah(matkul);
        mataKuliahService.deleteMataKuliah(matkul.getKodeMatkul());
        Assertions.assertEquals(null, mataKuliahService.getMataKuliah(matkul.getKodeMatkul()));
    }

    @Test
    void testServiceDeleteMataKuliahExists() {
        mataKuliahService.createMataKuliah(matkul);
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        mataKuliahService.deleteMataKuliah(matkul.getKodeMatkul());
        verify(mataKuliahRepository, times(1)).delete(matkul);
    }

    @Test
    public void testServiceDeleteMataKuliahThatDoesNotExist(){
        assertEquals(null, mataKuliahService.getMataKuliah("-1"));
        mataKuliahService.deleteMataKuliah("-1");
        assertEquals(null, mataKuliahService.getMataKuliah("-1"));
    }

    @Test
    void testServiceDeleteInvalidMataKuliah() {
        mahasiswaService.createMahasiswa(mahasiswa);
        mataKuliahService.createMataKuliah(matkul);
        mataKuliahService.daftarAsisten(mahasiswa, matkul);
        mataKuliahService.deleteMataKuliah(matkul.getKodeMatkul());
        lenient().when(mataKuliahService.getMataKuliah(matkul.getKodeMatkul())).thenReturn(matkul);
        Assertions.assertEquals(matkul, mataKuliahService.getMataKuliah(matkul.getKodeMatkul()));
    }

    @Test
    void testServiceDaftarValidAsisten() {
        mahasiswaService.createMahasiswa(mahasiswa);
        mataKuliahService.createMataKuliah(matkul);
        mataKuliahService.daftarAsisten(mahasiswa, matkul);
        Assertions.assertEquals(mahasiswa.getMataKuliah(), matkul);
    }

    @Test
    void testServiceDaftarInValidAsisten() {
        MataKuliah newMatkul = new MataKuliah("BUKAN_ADVPROG", "Advanced Programming", "Ilmu Komputer");
        mahasiswaService.createMahasiswa(mahasiswa);
        mataKuliahService.createMataKuliah(matkul);

        mahasiswa.setMataKuliah(matkul);

        mataKuliahService.daftarAsisten(mahasiswa, newMatkul);
        Assertions.assertEquals(mahasiswa.getMataKuliah(), matkul);
        Assertions.assertNotEquals(mahasiswa.getMataKuliah(), newMatkul);
    }
}
