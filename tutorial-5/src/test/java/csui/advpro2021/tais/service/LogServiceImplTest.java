package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class LogServiceImplTest {
    @Mock
    private LogRepository logRepository;

    @InjectMocks
    private LogServiceImpl logService;

    @Mock
    private MahasiswaService mahasiswaService;

    @Mock
    private MataKuliahService mataKuliahService;

    private Log log;
    private Mahasiswa mahasiswa;
    private MataKuliah matkul;

    @BeforeEach
    public void setUp() throws ParseException {
        mahasiswa = new Mahasiswa();
        mahasiswa.setNpm("1906192052");
        mahasiswa.setNama("Maung Meong");
        mahasiswa.setEmail("maung@cs.ui.ac.id");
        mahasiswa.setIpk("4");
        mahasiswa.setNoTelp("081317691718");

        log = new Log();
        DateFormat format = new SimpleDateFormat("d-M-yyyy", Locale.ENGLISH);
        Date start = format.parse("03-03-2021");
        Date endTime = format.parse("07-03-2021");
        log.setStart(start);
        log.setEndTime(endTime);
        log.setDeskripsi("deskripsi");

        matkul = new MataKuliah("ADVPROG", "Advanced Programming", "Ilmu Komputer");
    }

    @Test
    public void testServiceCreateLogNonAsistingMahasiswa(){
        lenient().when(logService.createLog(mahasiswa, log)).thenReturn(log);
    }

    @Test
    public void testServiceCreateLogAsistingMahasiswa(){
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswa.setMataKuliah(matkul);
        Log resultLog = logService.createLog(mahasiswa, log);
        assertEquals(resultLog.getIdLog(), log.getIdLog());
        assertEquals(resultLog.getMahasiswa(), mahasiswa);
    }

    @Test
    public void testServiceGetMahasiswaListLog(){
        mahasiswaService.createMahasiswa(mahasiswa);
        mahasiswa.setMataKuliah(matkul);
        logService.createLog(mahasiswa, log);

        // Expected response
        List<HashMap> responseList = new ArrayList<>();
        HashMap<String, Object> responseItem = new HashMap<>();
        double jamKerja = ((log.getEndTime().getTime() - log.getStart().getTime())/(60 * 60 * 1000.0));
        DateFormat format = new SimpleDateFormat("MMMMM", Locale.ENGLISH);
        responseItem.put("month", format.format(log.getStart()));
        responseItem.put("jamKerja", jamKerja);
        responseItem.put("pembayaran", jamKerja * 350);
        responseList.add(responseItem);

        Iterable<HashMap> generatedResponse = logService.getMahasiswaLogs(mahasiswa);

        Assertions.assertIterableEquals(responseList, generatedResponse);
    }

    @Test
    public void testServiceGetLogById(){
        lenient().when(logService.getLogById(log.getIdLog())).thenReturn(log);
        Log resultLog = logService.getLogById(log.getIdLog());
        assertEquals(log.getIdLog(), resultLog.getIdLog());
    }

    @Test
    public void testServiceDeleteLog(){
        logService.createLog(mahasiswa, log);
        logService.deleteLogById(log.getIdLog());
        lenient().when(logService.getLogById(log.getIdLog())).thenReturn(null);
        assertEquals(null, logService.getLogById(log.getIdLog()));
    }

    @Test
    public void testServiceUpdateLog(){
        logService.createLog(mahasiswa, log);
        String currentDeskripsi = log.getDeskripsi();
        String newDeskripsi = "newDeskripsi";
        //Change deskripsi to newDeskripsi
        log.setDeskripsi(newDeskripsi);

        lenient().when(logService.updateLog(log.getIdLog(), log)).thenReturn(log);
        Log resultLog = logService.updateLog(log.getIdLog(), log);

        assertNotEquals(resultLog.getDeskripsi(), currentDeskripsi);
        assertEquals(resultLog.getDeskripsi(), newDeskripsi);
        assertEquals(resultLog.getStart(), log.getStart());


    }

}
