package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.MataKuliahService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mata-kuliah")
public class MataKuliahController {
    @Autowired
    private MataKuliahService mataKuliahService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<MataKuliah>> getListMataKuliah() {
        return ResponseEntity.ok(mataKuliahService.getListMataKuliah());
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity createMataKuliah(@RequestBody MataKuliah mataKuliah) {
        return ResponseEntity.ok(mataKuliahService.createMataKuliah(mataKuliah));
    }

    /**
     * Endpoint untuk mendaftarkan mahasiswa pada suatu mata kuliah
     * */
    @PostMapping(path = "/daftar-asisten/{npm}/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity daftarAsisten(@PathVariable(value = "npm") String npm, @PathVariable(value = "kodeMatkul") String kodeMatkul) {
        MataKuliah matkul = mataKuliahService.getMataKuliah(kodeMatkul);
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (matkul == null || mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        } else if (mahasiswa.getMataKuliah() != null) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else {
            mataKuliahService.daftarAsisten(mahasiswa, matkul);
            return new ResponseEntity(HttpStatus.OK);
        }
    }

    @GetMapping(path = "/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMataKuliah(@PathVariable(value = "kodeMatkul") String kodeMatkul) {
        MataKuliah matkul = mataKuliahService.getMataKuliah(kodeMatkul);
        if (matkul == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(matkul);
    }

    @PutMapping(path = "/{kodeMatkul}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity updateMataKuliah(@PathVariable(value = "kodeMatkul") String kodeMatkul, @RequestBody MataKuliah mataKuliah) {
        return ResponseEntity.ok(mataKuliahService.updateMataKuliah(kodeMatkul, mataKuliah));
    }

    @DeleteMapping(path = "/{kodeMatkul}", produces = {"application/json"})
    public ResponseEntity deleteMataKuliah(@PathVariable(value = "kodeMatkul") String kodeMatkul) {
        mataKuliahService.deleteMataKuliah(kodeMatkul);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
