package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Log createLog(Mahasiswa mahasiswa, Log log) {
        if (mahasiswa.getMataKuliah() != null) {
            log.setMahasiswa(mahasiswa);
            logRepository.save(log);
            mahasiswa.getLogList().add(log);
            mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);
        }
        return log;
    }

    @Override
    public Log getLogById(int idLog) {
        return logRepository.findByIdLog(idLog);
    }

    @Override
    public Iterable<HashMap> getMahasiswaLogs(Mahasiswa mahasiswa) {
        List<Log> logList = mahasiswa.getLogList();
        List<HashMap> responseList = new ArrayList<>();
        HashMap<String, HashMap<String, Double>> store = new HashMap<>();
        for (int i = 0; i < logList.size(); i++) {
            Log curLog = logList.get(i);
            Date start = curLog.getStart();
            Date end = curLog.getEndTime();
            DateFormat format = new SimpleDateFormat("MMMMM", Locale.ENGLISH);
            String month = format.format(start);
            double jamKerja = ((end.getTime() - start.getTime()) / (60 * 60 * 1000.0));
            double pembayaran = 350 * jamKerja;
            if (!store.containsKey(month)) {
                HashMap<String, Double> init = new HashMap<>();
                init.put("jamKerja", 0.0);
                init.put("pembayaran", 0.0);
                store.put(month, init);
            }
            HashMap<String, Double> item = store.get(month);
            item.put("jamKerja", item.get("jamKerja") + jamKerja);
            item.put("pembayaran", item.get("pembayaran") + pembayaran);
            store.put(month, item);
        }
        HashMap<String, Object> response;
        for (String key: store.keySet()) {
            HashMap<String, Double> values = store.get(key);
            response = new HashMap<>();
            response.put("month", key);
            response.put("jamKerja", values.get("jamKerja"));
            response.put("pembayaran", values.get("pembayaran"));
            responseList.add(response);
        }
        return responseList;
    }

    @Override
    public Log updateLog(int idLog, Log log) {
        log.setIdLog(idLog);
        logRepository.save(log);
        return log;
    }

    @Override
    public void deleteLogById(int idLog) {
        logRepository.deleteById(idLog);
    }
}
