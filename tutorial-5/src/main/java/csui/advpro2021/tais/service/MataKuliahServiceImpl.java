package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import csui.advpro2021.tais.repository.MahasiswaRepository;
import csui.advpro2021.tais.repository.MataKuliahRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MataKuliahServiceImpl implements MataKuliahService {
    @Autowired
    private MataKuliahRepository mataKuliahRepository;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Override
    public Iterable<MataKuliah> getListMataKuliah() {
        return mataKuliahRepository.findAll();
    }

    @Override
    public MataKuliah getMataKuliah(String kodeMatkul) {
        return mataKuliahRepository.findByKodeMatkul(kodeMatkul);
    }

    @Override
    public MataKuliah createMataKuliah(MataKuliah mataKuliah) {
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public MataKuliah updateMataKuliah(String kodeMatkul, MataKuliah mataKuliah) {
        mataKuliah.setKodeMatkul(kodeMatkul);
        mataKuliahRepository.save(mataKuliah);
        return mataKuliah;
    }

    @Override
    public void deleteMataKuliah(String kodeMatkul) {
        MataKuliah matkul = this.getMataKuliah(kodeMatkul);
        if (matkul != null && matkul.getAsistenList().size() == 0) {
            mataKuliahRepository.delete(matkul);
        }
    }

    @Override
    public void daftarAsisten(Mahasiswa mahasiswa, MataKuliah mataKuliah) {
        if (mahasiswa.getMataKuliah() == null) {
            mahasiswa.setMataKuliah(mataKuliah);
            mataKuliah.getAsistenList().add(mahasiswa);
            mahasiswaService.updateMahasiswa(mahasiswa.getNpm(), mahasiswa);
            this.updateMataKuliah(mataKuliah.getKodeMatkul(), mataKuliah);
        }
    }
}
