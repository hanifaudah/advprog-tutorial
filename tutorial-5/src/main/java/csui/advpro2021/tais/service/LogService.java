package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

public interface LogService {
    Log createLog(Mahasiswa mahasiswa, Log log);

    Log getLogById(int idLog);

    Iterable<HashMap> getMahasiswaLogs(Mahasiswa mahasiswa);

    Log updateLog(int idLog, Log log);

    void deleteLogById(int idLog);
}
