package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.MataKuliah;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;

public interface MataKuliahService {
    Iterable<MataKuliah> getListMataKuliah();

    MataKuliah createMataKuliah(MataKuliah mataKuliah);

    MataKuliah getMataKuliah(String kodeMatkul);

    MataKuliah updateMataKuliah(String kodeMatkul, MataKuliah mataKuliah);

    void deleteMataKuliah(String kodeMatkul);

    void daftarAsisten(Mahasiswa mahasiswa, MataKuliah mataKuliah);
}
