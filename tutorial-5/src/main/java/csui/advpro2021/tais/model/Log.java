package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@JsonIgnoreProperties(value = {
        "mahasiswa"
})

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
//@AllArgsConstructor
public class Log {

    @Id
    @GeneratedValue
    @Column(name = "id_log", updatable = false, nullable = false)
    private int idLog;

    @Column(name = "start", nullable = false)
    private Date start;

    @Column(name = "end_time", nullable = false)
    private Date endTime;

    @Column(name ="deskripsi", nullable = false)
    private String deskripsi;

//    public Log(int _idLog, Date _start, Date _endTime, String _deskripsi) {
//        this.idLog = _idLog;
//        this.start = _start;
//        this.endTime = _endTime;
//        this.deskripsi = _deskripsi;
//    }

    public Log(Date _start, Date _endTime, String _deskripsi) {
        this.start = _start;
        this.endTime = _endTime;
        this.deskripsi = _deskripsi;
    }

    @ManyToOne
    private Mahasiswa mahasiswa;
}

