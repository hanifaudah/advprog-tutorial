package id.ac.ui.cs.advprog.tutorial1.observer.service;

import id.ac.ui.cs.advprog.tutorial1.observer.core.*;
import id.ac.ui.cs.advprog.tutorial1.observer.repository.QuestRepository;
import id.ac.ui.cs.advprog.tutorial1.observer.core.Adventurer;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GuildServiceImpl implements GuildService {

    private final QuestRepository questRepository;
    private final Guild guild;
    private final Adventurer agileAdventurer;
    private final Adventurer knightAdventurer;
    private final Adventurer mysticAdventurer;

    public GuildServiceImpl(QuestRepository questRepository) {
        this.questRepository = questRepository;
        this.guild = new Guild();

        // Initialize adventurers
        this.agileAdventurer = new AgileAdventurer(guild);
        this.knightAdventurer = new KnightAdventurer(guild);
        this.mysticAdventurer = new MysticAdventurer(guild);

        // Add all adventurers to guild
        guild.add(agileAdventurer);
        guild.add(knightAdventurer);
        guild.add(mysticAdventurer);
    }

    /**
     * Implementation of getAdventurers abstract method
     * */
    public List<Adventurer> getAdventurers() {
        return guild.getAdventurers();
    }

    /**
     * Implementation of addQuest abstract method
     * */
    public void addQuest(Quest quest) {
        // Only add quest of quest.title and quest.type is not empty
        if (quest.getTitle().length() > 0 && quest.getType().length() > 0 && questRepository.getQuests().get(quest.getTitle()) == null) {
            questRepository.save(quest);
            guild.addQuest(quest);
        }
    }
}
