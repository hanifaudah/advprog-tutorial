package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    /**
     * attack implementation
     * */
    @Override
    public String attack() {
        return "SIMSALABIM";
    }

    /**
     * getter for type attribute
     * */
    @Override
    public String getType() {
        return "MAGIC";
    }
}
