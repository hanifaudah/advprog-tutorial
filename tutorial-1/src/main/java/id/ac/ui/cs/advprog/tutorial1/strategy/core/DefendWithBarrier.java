package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    /**
     * defend implementation
     * */
    @Override
    public String defend() {
        return "Barrierku tidak dapat diterobos!";
    }

    /**
     * getter for type attribute
     * */
    @Override
    public String getType() {
        return "BARRIER";
    }
}
