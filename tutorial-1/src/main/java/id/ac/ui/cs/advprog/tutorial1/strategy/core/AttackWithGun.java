package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
    /**
     * attack implementation
     * */
    @Override
    public String attack() {
        return "DORRR";
    }

    /**
     * getter for type attribute
     * */
    @Override
    public String getType() {
        return "GUN";
    }
}
