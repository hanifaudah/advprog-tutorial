package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    /**
     * defend implementation
     * */
    @Override
    public String defend() {
        return "Armorku kuat";
    }

    /**
     * getter for type attribute
     * */
    @Override
    public String getType() {
        return "ARMOR";
    }
}
