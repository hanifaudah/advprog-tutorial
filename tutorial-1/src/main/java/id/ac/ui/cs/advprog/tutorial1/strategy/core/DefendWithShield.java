package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    /**
     * defend implementation
     * */
    @Override
    public String defend() {
        return "PERISAAAI";
    }

    /**
     * getter for type attribute
     * */
    @Override
    public String getType() {
        return "SHIELD";
    }
}
