package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
    /**
     * attack implementation
     * */
    @Override
    public String attack() {
        return "SWOOSH";
    }

    /**
     * getter for type attribute
     * */
    @Override
    public String getType() {
        return "SWORD";
    }
}
