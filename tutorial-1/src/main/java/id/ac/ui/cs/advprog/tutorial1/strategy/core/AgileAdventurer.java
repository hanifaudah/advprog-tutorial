package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
    public AgileAdventurer() {
        // Set initial attack and defend behaviors
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    /**
     * getter for alias
     * */
    @Override
    public String getAlias() {
        return "AGILE";
    }
}
