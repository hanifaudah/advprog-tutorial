package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
    public KnightAdventurer() {
        // Set initial attack and defend behaviors
        this.setAttackBehavior(new AttackWithSword());
        this.setDefenseBehavior(new DefendWithArmor());
    }

    /**
     * getter for alias
     * */
    @Override
    public String getAlias() {
        return "KNIGHT";
    }
}
