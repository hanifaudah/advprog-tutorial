package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
    }

    /**
     * Update method for Knight
     * Knight accepts all quests
     * */
    @Override
    public void update() {
        this.getQuests().add(this.guild.getQuest());
    }
}
