package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
    public MysticAdventurer() {
        // Set initial attack and defend behaviors
        this.setAttackBehavior(new AttackWithMagic());
        this.setDefenseBehavior(new DefendWithShield());
    }

    /**
     * getter for alias
     * */
    @Override
    public String getAlias() {
        return "MYSTIC";
    }
}
