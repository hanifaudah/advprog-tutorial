package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    @Override
    public void update() {
        // Mystic accepts only DELIVERY and ESCORT quests
        if (
                this.guild.getQuestType().equals("D") ||
                this.guild.getQuestType().equals("E")
        ) this.getQuests().add(this.guild.getQuest());
    }
}
