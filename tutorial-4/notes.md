# Perbandingan Instansiasi *Lazy* dan *Eager*
## *Lazy Instatiation*
Instansiasi *lazy* merupakan metode pembuatan objek dari
kelas Singleton dengan melakukan pemeriksaan apakah
atribut yang menampung *instance* bernilai `null`. Jika atribut tersebut
bernilai `null`, maka objek baru akan diciptakan menggunakan
operator `new`. Pemeriksaan nilai `null` dan penciptaan objek terjadi
dalam konstruktor dari kelas Singleton.

- Kelebihan
    - Pembuatan objek dapat dilakukan hanya bila perlu.
    - Menghemat memori karena tidak melakukan instansiasi terhadap objek
    yang belum diperlukan.
- Kekurangan 
    - Akses *instance* pertama pada kelas Singleton 
    akan membutuhkan waktu yang relatif lama karena harus menunggu
    proses instansiasi objek.

## *Eager Instatiation*
Pada metode instansiasi ini, penciptaan objek dilakukan
secara langsung pada deklarasi atribut.

- Kelebihan
    - Mudah diimplementasi.
    - Waktu akses selalu cepat karena objek langsung diinstansiasi
    sejak awal.
- Kekurangan
    - Menggunakan sumber daya untuk menginstansiasi objek 
    walaupun objek belum diperlukan. Hal ini dikarenakan 
    instansiasi dilakukan secara langsung pada deklarasi atribut.

## Perbedaan Instansias *Lazy* dan *Eager*
Perbedaan instansiasi *lazy* dengan *eager* terletak pada waktu
pembuatan objek. Pada instansiasi *eager*, objek terbuat saat kelas diload.
Berbeda dengan instansiasi *lazy* yang membuat objek saat method `getInstance()`
pertama kali dipanggil. 