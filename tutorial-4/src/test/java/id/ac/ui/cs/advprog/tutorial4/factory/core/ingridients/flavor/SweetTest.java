package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SweetTest {
    final String response = "Adding a dash of Sweet Soy Sauce...";

    @Test
    public void testGetDescription() {
        Flavor flavor = new Sweet();
        assertEquals(flavor.getDescription(), response);
    }
}
