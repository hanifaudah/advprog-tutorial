package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UmamiTest {
    final String response = "Adding WanPlus Specialty MSG flavoring...";

    @Test
    public void testGetDescription() {
        Flavor flavor = new Umami();
        assertEquals(flavor.getDescription(), response);
    }
}
