package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Umami;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Fish;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Shirataki;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Flower;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SnevnezhaShiratakiTest {
    private final String menuName = "menuName";
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;

    @BeforeEach
    public void setUp() throws Exception {
        //Ingridients:
        //Noodle: Shirataki
        //Meat: Fish
        //Topping: Flower
        //Flavor: Umami
        noodle = new Shirataki();
        meat = new Fish();
        topping = new Flower();
        flavor = new Umami();
    }

    @Test
    public void testIngredients() {
        Menu menu = new SnevnezhaShirataki(menuName);

        // Check attributs
        assertEquals(menu.getName(), menuName);
        assertEquals(menu.getNoodle().getDescription(), noodle.getDescription());
        assertEquals(menu.getMeat().getDescription(), meat.getDescription());
        assertEquals(menu.getTopping().getDescription(), topping.getDescription());
        assertEquals(menu.getFlavor().getDescription(), flavor.getDescription());
    }
}
