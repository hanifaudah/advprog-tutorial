package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PorkTest {
    final String response = "Adding Tian Xu Pork Meat...";

    @Test
    public void testGetDescription() {
        Meat meat = new Pork();
        assertEquals(meat.getDescription(), response);
    }
}
