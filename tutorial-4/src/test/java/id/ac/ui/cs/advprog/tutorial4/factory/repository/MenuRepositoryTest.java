package id.ac.ui.cs.advprog.tutorial4.factory.repository;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.Menu;
import id.ac.ui.cs.advprog.tutorial4.factory.service.MenuServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class MenuRepositoryTest {
    MenuRepository menuRepository;
    MenuServiceImpl menuService;

    private List<Menu> menus;

    @BeforeEach
    public void setup() throws Exception {
        menuRepository = new MenuRepository();
        menuService = new MenuServiceImpl(menuRepository);
        menus = (ArrayList<Menu>) ReflectionTestUtils.getField(menuRepository, "list");
    }

    @Test
    public void whenMenuRepoGetMenusItShouldReturnList() {
        List<Menu> acquiredMenus = menuRepository.getMenus();
        assertThat(acquiredMenus).isEqualTo(menus);
    }

    @Test
    public void whenMenuRepoAddItShouldAddMenu() {
        Menu newMenu = new InuzumaRamen("Ramen");
        menuRepository.add(newMenu);
        ArrayList<Menu> newMenuList = (ArrayList<Menu>) ReflectionTestUtils.getField(menuRepository, "list");
        assertNotEquals(newMenuList.indexOf(newMenu), -1);
    }
}
