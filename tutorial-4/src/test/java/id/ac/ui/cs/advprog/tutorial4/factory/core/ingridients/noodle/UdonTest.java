package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UdonTest {
    final String response = "Adding Mondo Udon Noodles...";

    @Test
    public void testGetDescription() {
        Noodle noodle = new Udon();
        assertEquals(noodle.getDescription(), response);
    }
}
