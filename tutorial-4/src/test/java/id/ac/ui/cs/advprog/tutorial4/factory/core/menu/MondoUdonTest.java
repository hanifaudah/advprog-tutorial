package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Chicken;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Cheese;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MondoUdonTest {
    private final String menuName = "menuName";
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;

    @BeforeEach
    public void setUp() throws Exception {
        //Ingridients:
        //Noodle: Udon
        //Meat: Chicken
        //Topping: Cheese
        //Flavor: Salty
        noodle = new Udon();
        meat = new Chicken();
        topping = new Cheese();
        flavor = new Salty();
    }

    @Test
    public void testIngredients() {
        Menu menu = new MondoUdon(menuName);

        // Check attributs
        assertEquals(menu.getName(), menuName);
        assertEquals(menu.getNoodle().getDescription(), noodle.getDescription());
        assertEquals(menu.getMeat().getDescription(), meat.getDescription());
        assertEquals(menu.getTopping().getDescription(), topping.getDescription());
        assertEquals(menu.getFlavor().getDescription(), flavor.getDescription());
    }
}
