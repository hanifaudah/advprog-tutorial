package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class OrderFoodTest {
    private Class<?> orderFoodClass;
    private OrderFood orderFood;
    private String foodName = "Suatu Makanan";

    @BeforeEach
    public void setUp() throws Exception {
        if (!Thread.currentThread().isAlive()) Thread.currentThread().start();
        orderFoodClass = Class.forName(OrderFood.class.getName());
        orderFood = OrderFood.getInstance();
    }

    /**
     * Test if only one object instance is created
     * */
    @Test
    public void testSingleInstance() {
        OrderFood instance1 = OrderFood.getInstance();
        OrderFood instance2 = OrderFood.getInstance();
        assertSame(instance1, instance2);
    }

    @Test
    public void testOrderFoodHasGetInstanceMethod() throws Exception {
        Method getInstance = orderFoodClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodHasGetFoodMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodHasSetFoodMethod() throws Exception {
        Method setFood = orderFoodClass.getDeclaredMethod("setFood", String.class);
        int methodModifiers = setFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodHasToStringMethod() throws Exception {
        Method toString = orderFoodClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSetFoodGetFoodToStringMethods() {
        orderFood.setFood(foodName);
        assertEquals(orderFood.getFood(), foodName);
        assertEquals(orderFood.toString(), foodName);
    }

    @Test
    public void testInterrupt() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<OrderFood> constructor;
        constructor = (Constructor<OrderFood>) OrderFood.class.getDeclaredConstructors()[0];
        constructor.setAccessible(true);
        Thread.currentThread().interrupt();
        constructor.newInstance();
    }
}
