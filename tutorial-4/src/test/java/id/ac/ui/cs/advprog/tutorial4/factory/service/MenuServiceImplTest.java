package id.ac.ui.cs.advprog.tutorial4.factory.service;

import id.ac.ui.cs.advprog.tutorial4.factory.core.menu.*;
import id.ac.ui.cs.advprog.tutorial4.factory.repository.MenuRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.atLeastOnce;

public class MenuServiceImplTest {
    private Class<?> menuServiceClass;
    private Class<?> ramenClass;
    private Class<?> sobaClass;
    private Class<?> udonClass;
    private Class<?> shiratakiClass;

    private List<Menu> menus;
    private final String menuName = "menuName";
    private final String sobaType = "Soba";
    private final String ramenType = "Ramen";
    private final String udonType = "Udon";
    private final String shiratakiType = "Shirataki";
    private int initialMenuRepoSize;

    MenuRepository menuRepository;

    MenuService menuService;

    @BeforeEach
    public void setup() throws Exception {
        ramenClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.InuzumaRamen");
        sobaClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.LiyuanSoba");
        udonClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.MondoUdon");
        shiratakiClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial4.factory.core.menu.SnevnezhaShirataki");

        menuRepository = new MenuRepository();
        menuService = new MenuServiceImpl(menuRepository);
        menus = (ArrayList<Menu>) ReflectionTestUtils.getField(menuRepository, "list");
        initialMenuRepoSize = menuService.getMenus().size();
    }

    @Test
    public void testDefaultConstructor() {
        MenuService testMenuService = new MenuServiceImpl();
        assertNotNull(ReflectionTestUtils.getField(testMenuService, "repo"));
    }

    @Test
    public void testDefaultCreateMenuMethod() {
        Menu menu = menuService.createMenu(menuName, "type");
        assertEquals(menu.getClass(), shiratakiClass);
    }

    @Test
    public void testCreateSoba() {
        Menu menu = menuService.createMenu(menuName, sobaType);
        assertEquals(menu.getClass(), sobaClass);
        assertEquals(menuService.getMenus().size(), initialMenuRepoSize + 1);
    }

    @Test
    public void testCreateRamen() {
        Menu menu = menuService.createMenu(menuName, ramenType);
        assertEquals(menu.getClass(), ramenClass);
        assertEquals(menuService.getMenus().size(), initialMenuRepoSize + 1);
    }

    @Test
    public void testCreateUdon() {
        Menu menu = menuService.createMenu(menuName, udonType);
        assertEquals(menu.getClass(), udonClass);
        assertEquals(menuService.getMenus().size(), initialMenuRepoSize + 1);
    }

    @Test
    public void testCreateShirataki() {
        Menu menu =  menuService.createMenu(menuName, shiratakiType);
        assertEquals(menu.getClass(), shiratakiClass);
        assertEquals(menuService.getMenus().size(), initialMenuRepoSize + 1);
    }

    @Test
    public void testGetMenusMethod() {
        assertEquals(menus, menuService.getMenus());
    }

    @Test
    public void testInitialRepoSize() {
        assertEquals(menuService.getMenus().size(), 4);
    }
}
