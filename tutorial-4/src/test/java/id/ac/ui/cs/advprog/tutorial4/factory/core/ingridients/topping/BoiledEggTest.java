package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Udon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BoiledEggTest {
    final String response = "Adding Guahuan Boiled Egg Topping";

    @Test
    public void testGetDescription() {
        Topping topping = new BoiledEgg();
        assertEquals(topping.getDescription(), response);
    }
}
