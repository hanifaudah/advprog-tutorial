package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderServiceImplTest {
    private final String drinkName = "drinkName";
    private final String foodName = "foodName";
    OrderServiceImpl orderService;
    OrderDrink orderDrink = OrderDrink.getInstance();
    OrderFood orderFood = OrderFood.getInstance();

    @BeforeEach
    public void setUp() {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void testGetDrink() {
        ReflectionTestUtils.setField(orderDrink, "drink", drinkName);
        assertEquals(drinkName, orderService.getDrink().getDrink());
    }

    @Test
    public void testGetFood() {
        ReflectionTestUtils.setField(orderFood, "food", foodName);
        assertEquals(foodName, orderService.getFood().getFood());
    }

    @Test
    public void testOrderDrink() {
        orderService.orderADrink(drinkName);
        assertEquals(drinkName, orderDrink.getDrink());
    }

    @Test
    public void testOrderFood() {
        orderService.orderAFood(foodName);
        assertEquals(foodName, orderFood.getFood());
    }
}
