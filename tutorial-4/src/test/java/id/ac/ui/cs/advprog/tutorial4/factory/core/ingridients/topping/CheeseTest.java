package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheeseTest {
    final String response = "Adding Shredded Cheese Topping...";

    @Test
    public void testGetDescription() {
        Topping topping = new Cheese();
        assertEquals(topping.getDescription(), response);
    }
}
