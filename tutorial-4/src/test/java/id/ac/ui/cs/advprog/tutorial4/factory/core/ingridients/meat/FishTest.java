package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    final String response = "Adding Zhangyun Salmon Fish Meat...";

    @Test
    public void testGetDescription() {
        Meat meat = new Fish();
        assertEquals(meat.getDescription(), response);
    }
}
