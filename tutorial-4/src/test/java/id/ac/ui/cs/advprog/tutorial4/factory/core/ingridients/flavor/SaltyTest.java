package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SaltyTest {
    final String response = "Adding a pinch of salt...";

    @Test
    public void testGetDescription() {
        Flavor flavor = new Salty();
        assertEquals(flavor.getDescription(), response);
    }
}
