package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ChickenTest {
    final String response = "Adding Wintervale Chicken Meat...";

    @Test
    public void testGetDescription() {
        Meat meat = new Chicken();
        assertEquals(meat.getDescription(), response);
    }
}
