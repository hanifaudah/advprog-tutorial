package id.ac.ui.cs.advprog.tutorial4.singleton.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class OrderDrinkTest {
    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;
    private String drinkName = "Suatu Minuman";

    @BeforeEach
    public void setUp() throws Exception {
        if (!Thread.currentThread().isAlive()) Thread.currentThread().start();
        orderDrinkClass = Class.forName(OrderDrink.class.getName());
        orderDrink = OrderDrink.getInstance();
    }

    /**
     * Test if only one object instance is created
     * */
    @Test
    public void testSingleInstance() {
        OrderDrink instance1 = OrderDrink.getInstance();
        OrderDrink instance2 = OrderDrink.getInstance();
        assertSame(instance1, instance2);
    }

    @Test
    public void testOrderDrinkHasGetInstanceMethod() throws Exception {
        Method getInstance = orderDrinkClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkHasGetDrinkMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkHasSetDrinkMethod() throws Exception {
        Method setDrink = orderDrinkClass.getDeclaredMethod("setDrink", String.class);
        int methodModifiers = setDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkHasToStringMethod() throws Exception {
        Method toString = orderDrinkClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testSetDrinkGetDrinkToStringMethods() {
        orderDrink.setDrink(drinkName);
        assertEquals(orderDrink.getDrink(), drinkName);
        assertEquals(orderDrink.toString(), drinkName);
    }

    @Test
    public void testInterrupt() throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Constructor<OrderDrink> constructor;
        constructor = (Constructor<OrderDrink>) OrderDrink.class.getDeclaredConstructors()[0];
        constructor.setAccessible(true);
        Thread.currentThread().interrupt();
        constructor.newInstance();
    }
}
