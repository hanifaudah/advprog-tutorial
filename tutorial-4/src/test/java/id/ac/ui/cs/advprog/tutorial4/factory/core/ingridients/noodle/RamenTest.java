package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RamenTest {
    final String response = "Adding Inuzuma Ramen Noodles...";

    @Test
    public void testGetDescription() {
        Noodle noodle = new Ramen();
        assertEquals(noodle.getDescription(), response);
    }
}
