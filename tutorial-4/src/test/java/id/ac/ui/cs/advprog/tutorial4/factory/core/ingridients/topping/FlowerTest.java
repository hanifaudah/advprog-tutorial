package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FlowerTest {
    final String response = "Adding Xinqin Flower Topping...";

    @Test
    public void testGetDescription() {
        Topping topping = new Flower();
        assertEquals(topping.getDescription(), response);
    }
}
