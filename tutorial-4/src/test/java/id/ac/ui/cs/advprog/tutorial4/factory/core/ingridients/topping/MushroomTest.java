package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MushroomTest {
    final String response = "Adding Shiitake Mushroom Topping...";

    @Test
    public void testGetDescription() {
        Topping topping = new Mushroom();
        assertEquals(topping.getDescription(), response);
    }
}
