package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiratakiTest {
    final String response = "Adding Snevnezha Shirataki Noodles...";

    @Test
    public void testGetDescription() {
        Noodle noodle = new Shirataki();
        assertEquals(noodle.getDescription(), response);
    }
}
