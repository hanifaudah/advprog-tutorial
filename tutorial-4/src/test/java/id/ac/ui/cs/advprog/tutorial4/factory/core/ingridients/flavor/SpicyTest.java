package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpicyTest {
    final String response = "Adding Liyuan Chili Powder...";

    @Test
    public void testGetDescription() {
        Flavor flavor = new Spicy();
        assertEquals(flavor.getDescription(), response);
    }
}
