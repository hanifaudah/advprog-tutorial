package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Salty;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BeefTest {
    final String response = "Adding Maro Beef Meat...";

    @Test
    public void testGetDescription() {
        Meat meat = new Beef();
        assertEquals(meat.getDescription(), response);
    }
}
