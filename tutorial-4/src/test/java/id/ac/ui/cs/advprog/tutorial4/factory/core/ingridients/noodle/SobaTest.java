package id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SobaTest {
    final String response = "Adding Liyuan Soba Noodles...";

    @Test
    public void testGetDescription() {
        Noodle noodle = new Soba();
        assertEquals(noodle.getDescription(), response);
    }
}
