package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Spicy;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Sweet;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Beef;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Pork;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Ramen;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Soba;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.BoiledEgg;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LiyuanSobaTest {
    private final String menuName = "menuName";
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;

    @BeforeEach
    public void setUp() throws Exception {
        //Ingridients:
        //Noodle: Soba
        //Meat: Beef
        //Topping: Mushroom
        //Flavor: Sweet
        noodle = new Soba();
        meat = new Beef();
        topping = new Mushroom();
        flavor = new Sweet();
    }

    @Test
    public void testIngredients() {
        Menu menu = new LiyuanSoba(menuName);

        // Check attributs
        assertEquals(menu.getName(), menuName);
        assertEquals(menu.getNoodle().getDescription(), noodle.getDescription());
        assertEquals(menu.getMeat().getDescription(), meat.getDescription());
        assertEquals(menu.getTopping().getDescription(), topping.getDescription());
        assertEquals(menu.getFlavor().getDescription(), flavor.getDescription());
    }
}
