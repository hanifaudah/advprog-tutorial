package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MyTransformationTest {
    private Class<?> myClass;

    @BeforeEach
    public void setup() throws Exception {
        myClass = Class.forName(
                "id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.MyTransformation");
    }

    @Test
    public void testMyTransformationHasEncodeMethod() throws Exception {
        Method translate = myClass.getDeclaredMethod("encode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTransformationEncodesCorrectly() throws Exception {
        String text = "Safira and I went to a blacksmith to forge our sword";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Fnsven naq V jrag gb n oynpxfzvgu gb sbetr bhe fjbeq";

        Spell result = new MyTransformation().encode(spell);
        assertEquals(expected, result.getText());
    }

    @Test
    public void testMyTransformationHasDecodeMethod() throws Exception {
        Method translate = myClass.getDeclaredMethod("decode", Spell.class);
        int methodModifiers = translate.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testMyTransformationDecodesCorrectly() throws Exception {
        String text = "Grfg qrpbqvat";
        Codex codex = AlphaCodex.getInstance();
        Spell spell = new Spell(text, codex);
        String expected = "Test decoding";

        Spell result = new MyTransformation().decode(spell);
        assertEquals(expected, result.getText());
    }
}