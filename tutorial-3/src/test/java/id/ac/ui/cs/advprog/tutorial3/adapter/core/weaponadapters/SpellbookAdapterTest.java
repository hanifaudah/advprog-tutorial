package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Heatbearer;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.TheWindjedi;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class SpellbookAdapterTest {
    private Class<?> spellbookAdapterClass;
    private Class<?> spellbookClass;

    private Spellbook mockHeatbearerSpellbook;
    private Spellbook mockTheWindjediSpellbook;
    private Weapon mockAdaptedHeatbearerSpellbook;
    private Weapon mockAdaptedTheWindjediSpellbook;
    private final String mockHolderName = "mockHolderName";
    private final String spellDepletedResponse = "Magic power not enough";

    @BeforeEach
    public void setUp() throws Exception {
        spellbookAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter");
        spellbookClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook");

        this.mockHeatbearerSpellbook = new Heatbearer(mockHolderName);
        this.mockTheWindjediSpellbook = new TheWindjedi(mockHolderName);
        this.mockAdaptedHeatbearerSpellbook = new SpellbookAdapter(this.mockHeatbearerSpellbook);
        this.mockAdaptedTheWindjediSpellbook = new SpellbookAdapter(this.mockTheWindjediSpellbook);
    }

    @Test
    public void testSpellbookAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(spellbookAdapterClass.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(spellbookAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testSpellbookAdapterConstructorReceivesSpellbookAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = spellbookClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                spellbookAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testSpellbookAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = spellbookAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = spellbookAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetNameMethod() throws Exception {
        Method getName = spellbookAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testSpellbookAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = spellbookAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testName() {
        assertEquals(mockAdaptedHeatbearerSpellbook.getName(), mockHeatbearerSpellbook.getName());
        assertEquals(mockAdaptedTheWindjediSpellbook.getName(), mockTheWindjediSpellbook.getName());
    }

    @Test
    public void testHolderName() {
        assertEquals(mockAdaptedHeatbearerSpellbook.getHolderName(), mockHeatbearerSpellbook.getHolderName());
        assertEquals(mockAdaptedTheWindjediSpellbook.getHolderName(), mockTheWindjediSpellbook.getHolderName());
    }

    @Test
    public void testNormalAttack() {
        assertEquals(mockAdaptedHeatbearerSpellbook.normalAttack(), mockHeatbearerSpellbook.smallSpell());
        assertEquals(mockAdaptedTheWindjediSpellbook.normalAttack(), mockTheWindjediSpellbook.smallSpell());
    }

    @Test
    public void testChargedAttack() {
        assertEquals(mockAdaptedHeatbearerSpellbook.chargedAttack(), mockHeatbearerSpellbook.largeSpell());
        assertEquals(mockAdaptedTheWindjediSpellbook.chargedAttack(), mockTheWindjediSpellbook.largeSpell());
    }

    @Test void testDepletedChargeAttack() {
        mockAdaptedHeatbearerSpellbook.chargedAttack();
        mockAdaptedTheWindjediSpellbook.chargedAttack();
        assertEquals(mockAdaptedHeatbearerSpellbook.chargedAttack(), spellDepletedResponse);
        assertEquals(mockAdaptedTheWindjediSpellbook.chargedAttack(), spellDepletedResponse);
    }
}
