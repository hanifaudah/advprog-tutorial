package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.IonicBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.UranosBow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.*;

// TODO: add tests
public class BowAdapterTest {
    private Class<?> bowAdapterClass;
    private Class<?> bowClass;

    private Bow mockIonicBow;
    private Bow mockUranosBow;
    private Weapon mockAdaptedIonicBow;
    private Weapon mockAdaptedUranosBow;
    private final String mockHolderName = "mockHolderName";
    private final String enterAimShotResponse = "Entered aim shot mode";
    private final String enterSpontanResponse = "Entered spontan mode";

    @BeforeEach
    public void setUp() throws Exception {
        bowAdapterClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter");
        bowClass = Class.forName("id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow");

        this.mockIonicBow = new IonicBow(mockHolderName);
        this.mockUranosBow = new UranosBow(mockHolderName);
        this.mockAdaptedIonicBow = new BowAdapter(this.mockIonicBow);
        this.mockAdaptedUranosBow = new BowAdapter(this.mockUranosBow);
    }

    @Test
    public void testBowAdapterIsConcreteClass() {
        assertFalse(Modifier.
                isAbstract(bowAdapterClass.getModifiers()));
    }

    @Test
    public void testBowAdapterIsAWeapon() {
        Collection<Type> interfaces = Arrays.asList(bowAdapterClass.getInterfaces());

        assertTrue(interfaces.stream()
                .anyMatch(type -> type.getTypeName()
                        .equals("id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon")));
    }

    @Test
    public void testBowAdapterConstructorReceivesBowAsParameter() {
        Class<?>[] classArg = new Class[1];
        classArg[0] = bowClass;
        Collection<Constructor<?>> constructors = Arrays.asList(
                bowAdapterClass.getDeclaredConstructors());

        assertTrue(constructors.stream()
                .anyMatch(type -> Arrays.equals(
                        type.getParameterTypes(), classArg)));
    }

    @Test
    public void testBowAdapterOverrideNormalAttackMethod() throws Exception {
        Method normalAttack = bowAdapterClass.getDeclaredMethod("normalAttack");

        assertEquals("java.lang.String",
                normalAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                normalAttack.getParameterCount());
        assertTrue(Modifier.isPublic(normalAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideChargedAttackMethod() throws Exception {
        Method chargedAttack = bowAdapterClass.getDeclaredMethod("chargedAttack");

        assertEquals("java.lang.String",
                chargedAttack.getGenericReturnType().getTypeName());
        assertEquals(0,
                chargedAttack.getParameterCount());
        assertTrue(Modifier.isPublic(chargedAttack.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetNameMethod() throws Exception {
        Method getName = bowAdapterClass.getDeclaredMethod("getName");

        assertEquals("java.lang.String",
                getName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getName.getParameterCount());
        assertTrue(Modifier.isPublic(getName.getModifiers()));
    }

    @Test
    public void testBowAdapterOverrideGetHolderMethod() throws Exception {
        Method getHolderName = bowAdapterClass.getDeclaredMethod("getHolderName");

        assertEquals("java.lang.String",
                getHolderName.getGenericReturnType().getTypeName());
        assertEquals(0,
                getHolderName.getParameterCount());
        assertTrue(Modifier.isPublic(getHolderName.getModifiers()));
    }

    // TODO: buat test untuk menguji hasil dari pemanggilan method
    @Test
    public void testName() {
        assertEquals(mockAdaptedIonicBow.getName(), mockIonicBow.getName());
        assertEquals(mockAdaptedUranosBow.getName(), mockUranosBow.getName());
    }

    @Test
    public void testHolderName() {
        assertEquals(mockAdaptedIonicBow.getHolderName(), mockIonicBow.getHolderName());
        assertEquals(mockAdaptedUranosBow.getHolderName(), mockUranosBow.getHolderName());
    }

    @Test
    public void testChangeMode() {
        assertEquals(mockAdaptedIonicBow.chargedAttack(), enterAimShotResponse);
        assertEquals(mockAdaptedUranosBow.chargedAttack(), enterAimShotResponse);

        assertEquals(mockAdaptedIonicBow.chargedAttack(), enterSpontanResponse);
        assertEquals(mockAdaptedUranosBow.chargedAttack(), enterSpontanResponse);
    }

    @Test
    public void testNormalAttackInAimShot() {
        assertEquals(mockAdaptedIonicBow.chargedAttack(), enterAimShotResponse);
        assertEquals(mockAdaptedUranosBow.chargedAttack(), enterAimShotResponse);

        assertEquals(mockAdaptedIonicBow.normalAttack(), mockIonicBow.shootArrow(true));
        assertEquals(mockAdaptedUranosBow.normalAttack(), mockUranosBow.shootArrow(true));
    }

    @Test
    public void testNormalAttackNotInAimShot() {
        assertEquals(mockAdaptedIonicBow.normalAttack(), mockIonicBow.shootArrow(false));
        assertEquals(mockAdaptedUranosBow.normalAttack(), mockUranosBow.shootArrow(false));
    }
}
