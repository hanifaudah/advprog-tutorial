package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

// TODO: Complete me. Modify this class as you see fit~
@Service
public class WeaponServiceImpl implements WeaponService {
    // feel free to include more repositories if you think it might help :)
    private boolean isFirstStartUp = true;

    // Repositories
    @Autowired
    private WeaponRepository weaponRepository;

    @Autowired
    private SpellbookRepository spellbookRepository;

    @Autowired
    private BowRepository bowRepository;

    @Autowired
    private LogRepository logRepository;

    private void initiateWeapons() {
        if (isFirstStartUp) {
            for (Spellbook spell: spellbookRepository.findAll()) weaponRepository.save(new SpellbookAdapter(spell));

            // Add bows
            for (Bow bow: bowRepository.findAll()) weaponRepository.save(new BowAdapter(bow));
            isFirstStartUp = !isFirstStartUp;
        }
    }

    // TODO: implement me
    @Override
    public List<Weapon> findAll() {
        initiateWeapons();
        return this.weaponRepository.findAll();
    }

    // TODO: implement me
    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        initiateWeapons();
        Weapon weapon = this.weaponRepository.findByAlias(weaponName);
        this.weaponRepository.save(weapon);
        if (weapon != null) {
            String response = "";
            String attackTypeStr = "";
            switch(attackType) {
                case 0:
                    response = weapon.normalAttack();
                    attackTypeStr = "normal attack";
                    break;
                case 1:
                    response = weapon.chargedAttack();
                    attackTypeStr = "charged attack";
                    break;
                default:
            }
            this.logRepository.addLog(String.format("%s attacked with %s (%s): %s", weapon.getHolderName(), weapon.getName(), attackTypeStr, response));
        }
    }

    // TODO: implement me
    @Override
    public List<String> getAllLogs() {
        initiateWeapons();
        return this.logRepository.findAll();
    }
}
