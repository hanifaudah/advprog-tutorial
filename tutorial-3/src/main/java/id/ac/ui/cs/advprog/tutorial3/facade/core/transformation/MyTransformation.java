package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class MyTransformation {
    public Spell encode(Spell spell) {return process(spell);}
    public Spell decode(Spell spell) {return process(spell);}

    private Spell process(Spell spell) {
        StringBuilder str = new StringBuilder(spell.getText());
        Codex codex = spell.getCodex();
        int diff = 13;
        for (int i = 0; i < str.length(); i++) {
            char cur = str.charAt(i);
            if (Character.isLetter(cur)) {
                int newVal;
                if (Character.isUpperCase(cur)) {
                    newVal = ((cur - 'A' + diff) % 26) + 'A';
                } else {
                    newVal = ((cur - 'a' + diff) % 26) + 'a';
                }
                str.setCharAt(i, (char) newVal);
            }
        }

        return new Spell(str.toString(), codex);
    }
}
