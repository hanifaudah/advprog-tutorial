package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class BowAdapter implements Weapon {
    private Bow bow;
    private boolean isAimShotState;

    public BowAdapter(Bow bow) {
        this.bow = bow;
    }

    @Override
    public String normalAttack() {
        return this.bow.shootArrow(this.isAimShotState);
    }

    @Override
    public String chargedAttack() {
        String response;
        this.isAimShotState = !this.isAimShotState;
        if (this.isAimShotState) response = "Entered aim shot mode";
        else response = "Entered spontan mode";

        return response;
    }

    @Override
    public String getName() {
        return this.bow.getName();
    }

    @Override
    public String getHolderName() {
        // TODO: complete me
        return this.bow.getHolderName();
    }
}
