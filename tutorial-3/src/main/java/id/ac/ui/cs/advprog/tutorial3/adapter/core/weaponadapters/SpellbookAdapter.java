package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;

// TODO: complete me :)
public class SpellbookAdapter implements Weapon {
    private boolean chargeState;
    private Spellbook spellbook;

    public SpellbookAdapter(Spellbook spellbook) {
        this.spellbook = spellbook;
        handleChargeState();
    }

    /**
     * Method that determines if charge attack is ready
     * */
    private void handleChargeState() {
        this.chargeState = !this.chargeState;
    }

    @Override
    public String normalAttack() {
        this.chargeState = true;
        return this.spellbook.smallSpell();
    }

    @Override
    public String chargedAttack() {
        String response;
        if (this.chargeState) response = this.spellbook.largeSpell();
        else response = "Magic power not enough";

        handleChargeState();

        return response;
    }

    @Override
    public String getName() {
        return this.spellbook.getName();
    }

    @Override
    public String getHolderName() {
        return this.spellbook.getHolderName();
    }

}
