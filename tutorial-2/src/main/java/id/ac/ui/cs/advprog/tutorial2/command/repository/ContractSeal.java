package id.ac.ui.cs.advprog.tutorial2.command.repository;

import id.ac.ui.cs.advprog.tutorial2.command.core.spell.Spell;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class ContractSeal {
    // Latest spell
    private Spell latestSpell;

    private Map<String, Spell> spells;

    public ContractSeal() {
        spells = new HashMap<>();
    }

    public void registerSpell(Spell spell) {
        spells.put(spell.spellName(), spell);
    }

    public void castSpell(String spellName) {
        // Get spell
        Spell currentSpell = this.spells.get(spellName);

        // Set spell as latest spell
        this.latestSpell = currentSpell;

        // Cast spell
        currentSpell.cast();
    }

    public void undoSpell() {
        // Undo latest spell if latest spell is not null
        if (latestSpell != null) {
            latestSpell.undo();

            // Prevent undo spam
            latestSpell = null;
        }
    }

    public Collection<Spell> getSpells() { return spells.values(); }
}
