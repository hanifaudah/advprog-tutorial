package id.ac.ui.cs.advprog.tutorial2.command.core.spell;
import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spellList;
    public ChainSpell(ArrayList<Spell> spellList) {
        this.spellList = spellList;
    }
    @Override
    public void cast() {
        // Cast all spells in spell list
        for (Spell s: this.spellList) s.cast();
    }

    @Override
    public void undo() {
        // Undo all spells starting from last
        for (int i = this.spellList.size() - 1; i >= 0; i--) this.spellList.get(i).undo();
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
