package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.Familiar;
import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.FamiliarState;

public abstract class FamiliarSpell implements Spell {
    protected Familiar familiar;

    @Override
    public void undo() {
        // Only undo if there is previous spell
        if (this.familiar.getLifeArchive().size() > 1) {
            if (familiar.getPrevState() == FamiliarState.ACTIVE) familiar.summon();
            else if (familiar.getPrevState() == FamiliarState.SEALED) familiar.seal();
        }
    }
}
